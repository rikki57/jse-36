package ru.nlmk.stury.jse36.first;

import java.util.List;

public interface WhereCreator {
    String createWhere(List<String> input);
}

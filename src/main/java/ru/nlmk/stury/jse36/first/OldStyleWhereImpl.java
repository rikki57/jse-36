package ru.nlmk.stury.jse36.first;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class OldStyleWhereImpl implements WhereCreator{
    public String createWhere(List<String> input) {
        if (input == null){
            return "";
        }
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < input.size(); i += 2) {
            if (i + 1 == input.size() || input.get(i + 1) == null){
                continue;
            }
            if (StringUtils.isNumeric(input.get(i + 1))){
                result.append(" and ").append(input.get(i)).append("=").append(input.get(i + 1));
            } else {
                result.append(" and ").append(input.get(i)).append("='").append(input.get(i + 1)).append("'");
            }
        }
        result.delete(0, 5);
        return result.toString();
    }
}

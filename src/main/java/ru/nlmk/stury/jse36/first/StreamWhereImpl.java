package ru.nlmk.stury.jse36.first;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StreamWhereImpl implements WhereCreator{
    @Override
    public String createWhere(List<String> input) {
        List<String> keys = IntStream.range(0, input.size())
                .filter(n -> n % 2 == 0)
                .mapToObj(input::get)
                .collect(Collectors.toList());
        List<String> values = IntStream.range(0, input.size())
                .filter(n -> n % 2 != 0)
                .mapToObj(input::get)
                .collect(Collectors.toList());
        return null;
    }
}

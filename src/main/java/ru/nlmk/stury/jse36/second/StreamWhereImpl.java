package ru.nlmk.stury.jse36.second;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StreamWhereImpl implements WhereCreator{
    @Override
    public String createWhere(List<Couple> input) {
        if (input == null || input.isEmpty()){
            return "";
        }
        input.removeIf(s -> s.getKey() == null || s.getValue() == null);
        String result = input.stream()
                .map(s -> " and " + s.getKey() + "=" + ValueMapper.getValue(s.getValue()))
                .collect(Collectors.joining());
        if (result.length() > 4){
            return result.substring(5);
        } else {
            return "";
        }
    }
}

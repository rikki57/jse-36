package ru.nlmk.stury.jse36.second;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Couple {
    private String key;
    private String value;
}

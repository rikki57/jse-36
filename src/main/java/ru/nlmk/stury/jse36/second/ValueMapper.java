package ru.nlmk.stury.jse36.second;

import org.apache.commons.lang3.StringUtils;

public class ValueMapper {
    public static String getValue(String input){
        if (StringUtils.isNumeric(input)){
            return input;
        } else {
            return "'" + input + "'";
        }
    }
}

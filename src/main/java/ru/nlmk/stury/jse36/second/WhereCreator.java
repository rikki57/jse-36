package ru.nlmk.stury.jse36.second;

import java.util.ArrayList;
import java.util.List;

public interface WhereCreator {
    String createWhere(List<Couple> input);
}

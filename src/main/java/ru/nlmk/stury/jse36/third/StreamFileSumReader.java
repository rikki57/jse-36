package ru.nlmk.stury.jse36.third;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class StreamFileSumReader implements FileSumReader{
    @Override
    public Long readFileSum(String fileName) {
        Path path = Paths.get(fileName);
        try (Stream<String> lines = Files.lines(path)){
            return lines.filter(StringUtils::isNumeric)
                    .mapToLong(Long::valueOf)
                    .sum();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0L;
    }
}

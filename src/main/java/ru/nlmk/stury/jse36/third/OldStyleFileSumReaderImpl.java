package ru.nlmk.stury.jse36.third;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class OldStyleFileSumReaderImpl implements FileSumReader {
    @Override
    public Long readFileSum(String fileName) {
        try(Scanner scanner = new Scanner(new File(fileName))){
            Long result = 0L;
            while (scanner.hasNextLine()){
                String line = scanner.nextLine();
                if (StringUtils.isNumeric(line)){
                    result += Long.valueOf(line);
                }
            }
            return result;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return 0L;
    }
}

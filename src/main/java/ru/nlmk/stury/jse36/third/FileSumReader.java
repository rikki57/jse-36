package ru.nlmk.stury.jse36.third;

public interface FileSumReader {
    Long readFileSum(String fileName);
}

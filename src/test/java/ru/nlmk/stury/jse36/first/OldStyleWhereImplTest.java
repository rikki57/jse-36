package ru.nlmk.stury.jse36.first;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class OldStyleWhereImplTest {
    @Test
    void testNormalCreateWhere(){
        String expectedResult = "name='Mike' and age=23 and city='Moscow'";
        List<String> input = Arrays.asList("name", "Mike", "age", "23", "city", "Moscow", "birth", null);
        WhereCreator whereCreator = new OldStyleWhereImpl();
        String result = whereCreator.createWhere(input);
        assertEquals(expectedResult, result);
    }

    @Test
    void testCreateWhereExtraParameter(){
        String expectedResult = "name='Mike' and age=23 and city='Moscow'";
        List<String> input = Arrays.asList("name", "Mike", "age", "23", "city", "Moscow", "birth");
        WhereCreator whereCreator = new OldStyleWhereImpl();
        String result = whereCreator.createWhere(input);
        assertEquals(expectedResult, result);
    }

    @Test
    void testCreateWhereEmptyList(){
        String expectedResult = "";
        List<String> input = new ArrayList<>();
        String result = new OldStyleWhereImpl().createWhere(input);
        assertEquals(expectedResult, result);
    }

    @Test
    void testCreateWhereNull(){
        String expectedResult = "";
        List<String> input = null;
        String result = new OldStyleWhereImpl().createWhere(input);
        assertEquals(expectedResult, result);
    }

    @Test
    void testCreateWhere1Element(){
        String expectedResult = "";
        List<String> input = Arrays.asList("name", null);
        String result = new OldStyleWhereImpl().createWhere(input);
        assertEquals(expectedResult, result);
    }
}
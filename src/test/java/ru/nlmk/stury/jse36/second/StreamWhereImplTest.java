package ru.nlmk.stury.jse36.second;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StreamWhereImplTest {
    @Test
    void testNormalCreateWhere(){
        String expectedResult = "name='Mike' and age=23 and city='Moscow'";
        List<Couple> input = Arrays.asList(
                new Couple("name", "Mike"),
                new Couple("age", "23"),
                new Couple("city", "Moscow"),
                new Couple("birth", null));
        WhereCreator whereCreator = new StreamWhereImpl();
        String result = whereCreator.createWhere(input);
        assertEquals(expectedResult, result);
    }

    @Test
    void testCreateWhereEmptyList(){
        String expectedResult = "";
        List<Couple> input = new ArrayList<>();
        String result = new StreamWhereImpl().createWhere(input);
        assertEquals(expectedResult, result);
    }

    @Test
    void testCreateWhereNull(){
        String expectedResult = "";
        List<Couple> input = null;
        String result = new StreamWhereImpl().createWhere(input);
        assertEquals(expectedResult, result);
    }

    @Test
    void testCreateWhere1Element(){
        String expectedResult = "";
        List<Couple> input = Arrays.asList(new Couple("name", null));
        String result = new StreamWhereImpl().createWhere(input);
        assertEquals(expectedResult, result);
    }
}
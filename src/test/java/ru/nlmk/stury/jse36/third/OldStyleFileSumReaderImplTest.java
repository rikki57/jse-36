package ru.nlmk.stury.jse36.third;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OldStyleFileSumReaderImplTest {

    @Test
    void readFileSum() {
        Long expected = 27L;
        Long result = new OldStyleFileSumReaderImpl().readFileSum("testdata//test1");
        assertEquals(expected, result);
    }

    @Test
    void readFileSumNoFile() {
        Long expected = 0L;
        Long result = new OldStyleFileSumReaderImpl().readFileSum("testdata//test2");
        assertEquals(expected, result);
    }
}
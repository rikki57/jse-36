package ru.nlmk.stury.jse36.third;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StreamFileSumReaderTest {
    @Test
    void readFileSum() {
        Long expected = 27L;
        Long result = new StreamFileSumReader().readFileSum("testdata//test1");
        assertEquals(expected, result);
    }

    @Test
    void readFileSumNoFile() {
        Long expected = 0L;
        Long result = new StreamFileSumReader().readFileSum("testdata//test2");
        assertEquals(expected, result);
    }
}